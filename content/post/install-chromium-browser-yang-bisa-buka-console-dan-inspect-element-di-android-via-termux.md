---
title: 'Install Chromium Browser Yang Bisa Buka Console Dan Inspect Element Di Android via Termux'
date: 2023-11-17T01:15:39Z
tags:
  - "termux"
  - "termux-x11"
  - "chromium"
---

Jika Anda seorang pengguna Android yang ingin membuka console browser, melakukan inspect element, atau menciptakan pengalaman seperti pada desktop browser, penggunaan Chromium di Termux dapat menjadi solusi yang ideal. Panduan ini khususnya cocok bagi mereka yang ingin melakukan debugging web frontend langsung dari perangkat Android. Chromium di Termux memberikan fungsionalitas yang mirip dengan desktop, termasuk akses ke console dan inspect element.

Langkah-langkah Instalasi Chromium di Termux:

1. Instal Termux: Pastikan Anda telah menginstal Termux, sebuah terminal emulator untuk lingkungan Linux di Android. Anda dapat mengunduh aplikasi ini melalui F-Droid atau GitHub.

2. Instal X11-Repo di Termux: Buka aplikasi Termux dan pasang x11-repo dengan menggunakan perintah berikut:
```
pkg install x11-repo
```
3. Instal Termux-X11-Nightly: Selanjutnya, instal termux-x11-nightly dengan perintah:
```
pkg install termux-x11-nightly
```
4. Instal Termux-X11 APK: Unduh termux-x11 APK [di sini](https://github.com/termux/termux-x11/actions) dan ekstrak. Install APK yang telah diekstrak.

5. Instal Tur-Repo di Termux: Kembali ke aplikasi Termux dan instal tur-repo menggunakan perintah:
```
pkg install tur-repo
```
6. Instal XFCE Desktop Environment: Pasang desktop environment XFCE dengan perintah:
```
pkg install xfce
```
7. Instal Chromium: Akhirnya, instal Chromium dengan perintah:
```
pkg install chromium
```
8. Memulai X11-Server: Setelah semuanya terinstal, jalankan perintah ini di Termux untuk memulai X11-server:
```
termux-x11 :0 -xstartup "dbus-launch --exit-with-session xfce4-session"
```
Selesai: Buka aplikasi Termux-X11, dan desktop Linux XFCE akan muncul. Pilih Chromium dari menu untuk menjalankan browser tersebut.

![termux x11 chromium](https://res.cloudinary.com/djuyt5yew/image/upload/v1700184300/e211895b-fcbe-454b-9615-fb61622ec2a8_jwvrwq.png)

Dengan mengikuti panduan ini, Anda dapat dengan mudah menginstal dan menjalankan Chromium di Termux, membuka peluang untuk eksplorasi dan pengembangan web langsung dari perangkat Android Anda. Semoga panduan ini membantu!