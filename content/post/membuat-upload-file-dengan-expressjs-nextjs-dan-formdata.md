---
title: 'Membuat Upload File Dengan Expressjs Nextjs Dan Formdata'
date: 2023-11-17T01:32:18Z
categories:
  - "Pemrograman"
tags:
  - "termux"
  - "next.j"
  - "express.js"
---

Halo friends!

Kita akan membuat file upload di frontend dengan next.js dan di backend dengan express.js.

Next.js adalah kerangka kerja React yang kuat yang menawarkan kemudahan dalam mengembangkan aplikasi frontend yang responsif dan berkualitas tinggi. Sementara itu, Express.js adalah kerangka kerja Node.js yang populer untuk pengembangan backend yang cepat dan efisien.

Contoh file upload adalah ketika kita disuruh upload pdf atau mengubah foto profil di akun sosmed kita.

Di frontend kita memilih menggunakan next.js karena kita akan menggunakan react.js dan FormData, sedangkan di backend kita akan memakai express.js dan multer middleware.

Multer middleware adalah middleware untuk express yang kegunaanya untuk memroses tipe data 'multipart/form-data'.

Dalam dunia yang didorong oleh teknologi saat ini, menggunakan aplikasi web yang mampu mengelola file upload secara efisien dapat meningkatkan pengalaman pengguna dan memberikan nilai tambah kepada pengguna. Dalam panduan ini, kita akan menjelajahi metode terbaik untuk mengimplementasikan file upload dengan Next.js dan Express.js.

Pertama-tama kita buat backend servernya dulu
```
mkdir server
cd server
npm init
npm install --save express multer
```
Penjelasan:

mkdir server: membuat folder untuk proyek kita bernama server.

cd server: berpindah folder aktif ke server.

npm init: mengubah folder kerja berbasis npm.

npm install: menginstal paket yang akan digunakan dalam proyek ini.

Kemudian buat file index.js untuk script backendnya, ini dia scriptnya:
```
const express = require('express')
const multer  = require('multer')
const cors = require('cors')
const upload = multer({ dest: 'uploads/' })

const app = express()
app.use(cors({origin: '*'}))

app.post('/', upload.single('file'), function (req, res, next) {
  console.log(req.file)
  res.status(200).send('success');
})

app.listen(3030)
```
Kita mengatur multernya untuk menyimpan file folder uploads kemudian mengaitkannya di endpoint / root, atau dasarnya domain kita dengan menerima http metode POST. upload.single('file') berarti multer diatur untuk menerima satu file saja saat upload dengan form data yang bernama file. Kemudian kita akan melihat datanya di req.file.

Jika sudah buat backendnya tentu saja sekarang adalah bagian frontendnya. Kita keluar dari folder server dan mulai buat folder baru:
```
npx create-next-app@latest uploads
```
Setelah keluar dari folder server kita membuat proyek Next.js bernama uploads, setelah semua perintah diikuti dan Next.js selesai menginstal buat form dengan file input page.tsx


```
"use client";
async function fileUpload(inputFile: any) {
  const formData = new FormData();
  formData.append('file', inputFile.target.files[0]);
  const resp = await fetch('http://localhost:3030/', {
    method: 'post',
    body: formData
  });

  if (resp.status === 200) {
    console.log(resp);
  }
}


export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <form>
        <input type="file" name="file" onChange={fileUpload} />
      </form>
    </main>
  )
}
```
Kita akan memakai FormData untuk mengirim filenya. Pada input file diatas kita buat listener onChange yang mengaitkan fungsi fileUpload. Di dalam fileUpload berisi metode-metode untuk mengupload file dengan fetch dan FormData.

Itu tadi upload file dengan express.js, Next.js dan FormData.