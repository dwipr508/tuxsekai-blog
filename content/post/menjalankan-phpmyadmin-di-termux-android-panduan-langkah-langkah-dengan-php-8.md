---
title: 'Menjalankan Phpmyadmin Di Termux Android Panduan Langkah Langkah Dengan Php 8'
date: 2023-11-17T01:05:08Z

tags:
  - "php"
  - "termux"
  - "phpmyadmin"
---

Panduan ini disusun untuk membantu Anda menjalankan phpMyAdmin di Termux Android dengan menggunakan PHP versi 8. Ikuti langkah-langkah berikut untuk menginstal program yang diperlukan dan mengonfigurasi pengaturan Apache.

Langkah-langkah Menjalankan phpMyAdmin di Termux:

Langkah 1: Install Program yang Diperlukan

Program-program berikut diperlukan untuk menjalankan phpMyAdmin di Termux:
```
pkg install php apache2 php-apache wget mariadb nano
```
Langkah 2: Edit httpd.conf dengan Nano

Gunakan editor Nano untuk mengedit file konfigurasi httpd.conf:
```
nano /data/data/com.termux/files/usr/etc/apache2/httpd.conf
```
Tambahkan snippet berikut setelah baris LoadModule:
```
LoadModule php_module libexec/apache2/libphp.so
<FilesMatch \.php$>
    SetHandler application/x-httpd-php
</FilesMatch>
LoadModule mpm_prefork_module libexec/apache2/mod_mpm_prefork.so
```
Jangan lupa untuk mengomentari modul mpm worker dengan menambahkan tanda # di depannya:
```
#LoadModule mpm_worker_module libexec/apache2/mod_mpm_worker.so
```
Dengan langkah-langkah ini, Anda telah mengonfigurasi Termux Anda untuk menjalankan phpMyAdmin dengan PHP versi 8. Simpan perubahan, dan restart Apache untuk menerapkan konfigurasi baru.

Berikut screenshot hasilnya:
![termux phpmyadmin](https://res.cloudinary.com/djuyt5yew/image/upload/v1696922776/Screenshot_2023-10-10-14-16-00_mfqqbg.png)
Tambahkan index.php di dir module
```
<IfModule dir_module>
    DirectoryIndex index.html index.php
</IfModule>
```
Langkah 3: download phpmyadmin dengan wget

Kita akan pindah folder ke htdocs
```
cd /$PREFIX/share/apache2/default-site/htdocs
```
Download phpmyadmin dengan wget
```
wget https://files.phpmyadmin.net/phpMyAdmin/5.2.1/phpMyAdmin-5.2.1-all-languages.zip
```
Unzip file phpmyadminnya
```
unzip phpMyAdmin-5.2.1-all-languages.zip
```
rename folder menjadi pma atau apapun boleh
```
mv -r phpMyAdmin-5.2.1-all-languages pma
```
Buka pma dan ubah config menjadi config.inc.php ubah juga hostnya menjadi 127.0.0.1
```
cp config.sample.inc.php config.inc.php
nano config.inc.php
```
![termux phpmyadmin](https://res.cloudinary.com/djuyt5yew/image/upload/v1696923556/Screenshot_2023-10-10-14-38-15_nmcqis.png)
Ok, Terakhir nyalakan `mysqld` dengan username root dan password.

Restart apachectl
```
apachectl stop
```
```
apachectl start
```
Buka browser di `12.7.0.0.1:8080/pma`

Selesai. Dengan mengikuti panduan ini, Anda dapat dengan mudah menjalankan phpMyAdmin di Termux Android, membuka pintu untuk pengembangan dan manajemen basis data secara lokal. Semoga panduan ini bermanfaat bagi Anda.